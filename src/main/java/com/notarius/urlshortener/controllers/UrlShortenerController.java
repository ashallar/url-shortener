package com.notarius.urlshortener.controllers;

import java.net.MalformedURLException;
import java.net.URL;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.notarius.urlshortener.library.UrlShortener;


@RestController
public class UrlShortenerController extends BaseController {

	/**
	 * Takes a complete url and provide a shortened url.
	 * @param url The complete url
	 * @return A string representing the shortened url
	 * @throws Exception 
	 * @throws IllegalArgumentException
	 */
	@RequestMapping("/shorten")
	public String ShortenUrl(@RequestParam("url") String url) throws Exception, IllegalArgumentException {
		try {
			URL myUrl = new URL(url);
			return UrlShortener.GetShortenedUrl(myUrl);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("The provided 'url' isn't formatted correctly");
		}
	}

	/**
	 * Takes the shortenedUrl id and returns the complete url or returns an error if the id doesn't exists.
	 * @param id The shortened url id
	 * @return A string representing the complete url
	 * @throws Exception
	 */
	@RequestMapping("/{id}")
	public String GetCompleteUrl(@PathVariable String id) throws Exception {
		return UrlShortener.GetCompleteUrl(id);
	}
}

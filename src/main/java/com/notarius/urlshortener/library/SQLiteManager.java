package com.notarius.urlshortener.library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author bastien
 * A static class to create and access the SQLite database
 */
public class SQLiteManager {

	private static String DB_NAME = "urlshortener.sqlite";

	private static String TABLE_NAME = "ID_URL_MATCH";

	private static String TABLE_FIELDNAME_ID = "ID";
	private static String TABLE_FIELDNAME_URL ="URL";

	private SQLiteManager() {
		/* Private constructor for static class */
	}


	/**
	 * Add a new entry for the shortened url.
	 * @param initialUrl The initial URL provided by the user.
	 * @param id The generated hash for the provided url.
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void AddUrlEntry(String initialUrl, String id) throws ClassNotFoundException, SQLException   {		
		Connection connection = null;
		Statement stmt = null;

			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);

			stmt = connection.createStatement();

			/* Try creating table if not exists to avoid missing table error */
			String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
					" ("+ TABLE_FIELDNAME_ID +"	TEXT	PRIMARY KEY     NOT NULL," +
					TABLE_FIELDNAME_URL + "	TEXT	NOT NULL)";
			stmt.executeUpdate(sql);


			sql = "INSERT INTO " + TABLE_NAME + 
					" ("+ TABLE_FIELDNAME_ID +", " + TABLE_FIELDNAME_URL + ") " +
					"VALUES ('" + id + "', " + "'" + initialUrl + "')";
			stmt.executeUpdate(sql);

			stmt.close();
			connection.close();
	}

	/**
	 * Takes a shortened url id and returns the complete url. Returns an error if the id isn't known.
	 * @param id The shortened url id
	 * @return A string representing the complete url.
	 * @throws Exception
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static String GetBaseUrlFromId(String id) throws ClassNotFoundException, SQLException {
		Connection connection = null;
		Statement stmt = null;

		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);

		stmt = connection.createStatement();

		ResultSet rs = stmt.executeQuery("SELECT * FROM " + TABLE_NAME + " WHERE ID = '" + id + "';");
		String result = rs.next() ? rs.getString(TABLE_FIELDNAME_URL) : "";

		rs.close();
		stmt.close();
		connection.close();
		
		return result;
	}
}

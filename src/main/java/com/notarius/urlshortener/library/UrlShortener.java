package com.notarius.urlshortener.library;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Bastien
 * A static class providing static methods to shorten url
 */
public final class UrlShortener {

	public static final int HASH_LENGTH = 10;
	public static final String MY_HOST = "http://localhost:8080/";

	private UrlShortener() {
		// A private constructor to disable object instantiation.
	}


	/**
	 * Takes a complete url in parameter and returns a shortened url. Also stores the information in the SQLite database.
	 * 
	 * @param url The complete url to be shortened.
	 * @return Returns the shortened url.
	 * @throws Exception The exception
	 * @throws MalformedURLException 
	 */
	public static String GetShortenedUrl(URL url) throws Exception {
		String hash = CryptographyUtils.Md5(url.toString()).substring(0, HASH_LENGTH);
		if (SQLiteManager.GetBaseUrlFromId(hash) == "") {
			SQLiteManager.AddUrlEntry(url.toString(), hash);
		}
		String result = MY_HOST + hash;

		return result;
	}

	/**
	 * Takes the id of a shortened url and send back the complete url if exists, throws an error the id doesn't exists.
	 * @param id The shortened url id
	 * @return The complete url for the provided id
	 * @throws Exception Throws exception if no
	 */
	public static String GetCompleteUrl(String id) throws Exception {
		String result = SQLiteManager.GetBaseUrlFromId(id);
		if (result == "") {
			throw new IllegalArgumentException("The provided 'id' is not known.");
		}
		return result;
	}
}

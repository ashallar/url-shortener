package com.notarius.urlshortener.library;

import java.nio.charset.StandardCharsets;

import org.springframework.util.DigestUtils;


/**
 * @author bastien
 * A custom utility class for cryptography
 */
public class CryptographyUtils {

	private CryptographyUtils() {
		// Private constructor to create static class.
	}

	/**
	 * Converts the string input into a Md5 hash.
	 * @param input The string input
	 * @return A string representing the md5 hash of the input
	 */
	public static String Md5(String input) {
		return DigestUtils.md5DigestAsHex(input.getBytes(StandardCharsets.UTF_8));
	}
}

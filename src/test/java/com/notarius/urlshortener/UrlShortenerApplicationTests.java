package com.notarius.urlshortener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UrlShortenerApplicationTests {

	@Test
	public void getShortenedUrl() throws Exception {
		String completeUrl = "http://google.com/test";
		String expectedShortenedUrl = "http://localhost:8080/bb1393b689";

		String result = sendGet("http://localhost:8080/shorten?" + completeUrl);
		Assert.assertSame(expectedShortenedUrl, result);
	}

	@Test
	public void sameUrlGivesSameShortenedUrl() throws Exception {
		String completeUrl = "http://google.com/test";
		
		String firstResult = sendGet("http://localhost:8080/shorten?" + completeUrl);
		String secondResult = sendGet("http://localhost:8080/shorten?" + completeUrl);

		Assert.assertSame(firstResult, secondResult);
	}
	
	@Test
	public void getCompleteUrl() throws Exception {
		String completeUrl = "http://google.com/test";

		String shortenedUrl = sendGet("http://localhost:8080/shorten?" + completeUrl);
		String retrievedCompleteUrl = sendGet(shortenedUrl);
		Assert.assertSame(completeUrl, retrievedCompleteUrl);
	}
	
	private String sendGet(String url) throws Exception {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}
}
